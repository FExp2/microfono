//Esta funcion se ejecuta solo una vez
//al iniciarse el Arduino. Aqui se colocan
//las instrucciones que tienen que ver con la configuracion.
void setup(){
  Serial.begin(9600); //Iniciamos el puerto serie
  //para comunicarse con la PC
  //a una velocidad de 9600 bauds.
  pinMode(8, INPUT);
  pinMode(13, OUTPUT);
}

//Esta funcion se ejecuta indefinidamente.
void loop(){
  int sensorValue = analogRead(A0); //Lee el valor del conversor A/D
  Serial.println(sensorValue); //Envia el valor leido a la PC mediante puerto serie
  int output = digitalRead(8); //Lee el valor digital del sensor de nivel.
  if (sensorValue > 200) { //Si el nivel de sonido supera un umbral, cambia el estado del LED.
    digitalWrite(13, !digitalRead(13)); //Conmuta el estado del LED cada vez que el nivel de sonido supera el umbral
    delay(100);
  }
  delay(10); //Espera 10ms
}
