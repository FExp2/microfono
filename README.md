# Micrófono

<img src="https://s3-ap-southeast-1.amazonaws.com/a2.datacaciques.com/wm/307176851/2951693848/3945107700.jpg" alt="fig. 1" width="300"/>

## Descripción

Este módulo contiene un [micrófono capacitivo](https://www.electronics-notes.com/articles/audio-video/microphones/condenser-capacitor-microphone.php)
y un [amplificador comparador](http://www.ti.com/lit/ds/symlink/lm393.pdf) para utilizarlo como interruptor por nivel de sonido. 

## Especificaciones

El módulo tiene 4 terminales (pines) de conexión:
- A0: salida analógica
- G: conectar a GND (ground)
- +: conectar a Vcc (+5V)
- D0: salida digital tipo switch configurable mediante el potenciómetro

## Diagrama de conexión con Arduino

<img src="img/Schematic.png" alt="fig. 1" width="450"/>

## Carga del firmware

Esta aplicación es muy sencilla. Lee los datos del sensor y los envía a la computadora para visualizarlos.

1. Descargar a su PC el [firmware](firmware/firmware.ino)
2. Abrir la aplicación [Arduino IDE](https://www.arduino.cc/en/software).

<img src="img/arduinoIDE_1.png" alt="fig. 1" width="300"/>

3. Abrir el código que descargó en el punto 1.
4. Conectar el Arduino a la computadora con el cable USB.
5. Seleccionar el puerto serie correspondiente al Arduino.

<img src="img/arduinoIDE_2.png" alt="fig. 1" width="300"/>

6. Seleccionar la placa Arduino que corresponde.

<img src="img/arduinoIDE_3.png" alt="fig. 1" width="300"/>

7. Cargar el firmware.
8. Abrir el *monitor serial* o el *serial plotter*

<img src="img/arduinoIDE_4.png" alt="fig. 1" width="300"/>

9. Asegurarse que la velocidad de comunicación está seteada en 9600 bauds

### AHORA APLAUDÍ CERCA DEL MICRÓFONO!!!
